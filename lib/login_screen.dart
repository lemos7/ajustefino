import 'package:ajustefino/home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Fine Ajustment"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(child: loginInside(context)),
    );
  }

  Widget loginInside(BuildContext context) {
    return SingleChildScrollView(
      
      child: Column(
        children: <Widget>[
          SizedBox(height: 30.0),
          Container(
              alignment: Alignment.center,
              child: Icon(
                Icons.person_pin,
                size: 150,
              )),
          SizedBox(height: 40),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 300,
                alignment: Alignment.center,
                child: TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      // icon: Icon(Icons.person, size: 30,),
                      hintText: 'Informe o usuário'),
                ),
              ),
            ],
          ),
          SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 300,
                alignment: Alignment.center,
                child: TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      // icon: Icon(Icons.lock, size: 30,),
                      hintText: 'Informe a senha'),
                ),
              ),
            ],
          ),
          SizedBox(height: 30.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  height: 60,
                  child: RaisedButton(
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    color: Colors.deepPurple,
                    child: Text(
                      "Confirmar",
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    ),
                    onPressed: () {//apenas para passar para a outra tela 
                      Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Home()
                      ),
                    );
                    },
                  )
                  ),
              SizedBox(
                width: 30,
              ),
              Container(
                  height: 60,
                  child: RaisedButton(
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    color: Colors.deepPurple,
                    child: Text(
                      "  Cancelar  ",
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    ),
                    onPressed: () {},
                  )),
            ],
          ),
        ],
      ),
    );
  }
}
