import 'package:flutter/material.dart';

class TireAdjustment extends StatefulWidget {
  @override
  _TireAdjustmentState createState() => _TireAdjustmentState();
}

class _TireAdjustmentState extends State<TireAdjustment> {
  TextEditingController pesoveiculoController = TextEditingController();
  TextEditingController pesoMaxPneuController = TextEditingController();
  TextEditingController calibMaxPneuController = TextEditingController();
  TextEditingController resulController = TextEditingController();

  String _infoText = "A calibragem ideal é: ";

  void resetFields() {//apagar os campos do textField
    setState(() {
      pesoveiculoController.text = "";
      pesoMaxPneuController.text = "";
      calibMaxPneuController.text = "";
      _infoText = "A calibragem ideal é: ";
    });
  }

  void calculate() {//calcular a calibragem do pneu
    setState(() {
      double pesoVeiculo = double.parse(pesoveiculoController.text);
      double pesoMaxPneu = double.parse(pesoMaxPneuController.text);
      double calibMaxPneu = double.parse(calibMaxPneuController.text);

      double resul = calibMaxPneu * pesoVeiculo / pesoMaxPneu;
      _infoText = "A calibragem ideal é: ${resul}";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("AJUSTE DOS PNEUS"),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.refresh),
              onPressed: () {
                resetFields();
              }),
          IconButton(
              icon: Icon(Icons.info),
              onPressed: () {
                _alertInfo(context);
              })
        ],
      ),
      body: SingleChildScrollView(child: screenInside(context)),
    );
  }

  Widget screenInside(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: <Widget>[
        SizedBox(height: 50),
        Container(
          child: Text(_infoText,
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.w600)),
        ),
        SizedBox(height: 30),
        Text("INFORME O PESO REAL DO VEÍCULO"),
        SizedBox(height: 5),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 300,
              alignment: Alignment.center,
              child: TextField(
                controller: pesoveiculoController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    // icon: Icon(Icons.person, size: 30,),
                    hintText: ''),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        Text("INFORME O PESO MÁXIMO QUE O PNEU SUPORTA"),
        SizedBox(height: 5),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 300,
              alignment: Alignment.center,
              child: TextField(
                controller: pesoMaxPneuController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    // icon: Icon(Icons.person, size: 30,),
                    hintText: ''),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        Text("INFORME A CALIBRAGEM MÁXIMA QUE O PNEUS SUPORTA"),
        SizedBox(height: 5),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 300,
              alignment: Alignment.center,
              child: TextField(
                controller: calibMaxPneuController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    // icon: Icon(Icons.person, size: 30,),
                    hintText: ''),
              ),
            ),
          ],
        ),
        SizedBox(height: 15),
        Container(
            height: 60,
            width: 150,
            child: RaisedButton(
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
              color: Colors.deepPurple,
              child: Text(
                "CALCULAR",
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              onPressed: () {
                calculate();
              },
            )),
      ],
    ));
  }

    Widget _alertInfo(BuildContext context) {

   Widget cancelaButton = FlatButton(
    child: Text("Entendi"),
    onPressed:  () { Navigator.of(context).pop();},
  );
  // Widget continuaButton = FlatButton(
  //   child: Text("Continar"),
  //   onPressed:  () {},
  // );

  //configura o AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Aonde encontrar essas informações ?"),
    content: Text("Estão nas laterais no pneu kkkk"),
    actions: [
      cancelaButton,
      // continuaButton,
    ],
  );

  //exibe o diálogo
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
}
