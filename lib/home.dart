import 'package:ajustefino/tires.dart';
import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Fine Ajustment"),
        centerTitle: true,
        actions: <Widget>[
          IconButton(icon: Icon(Icons.info), onPressed: (){_alertManual(context);})
        ],
      ),

       body: Container(
        child:  ListView.builder(
          itemCount: 3,   //número de vezes que você deseja replicar o widget
            itemBuilder: (context, index){
              return homeInside(context);
        }),
    )
    );
  }

  Widget homeInside(BuildContext context){
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(1, 3, 1, 3),
          alignment: Alignment.center,
          height: 178,
          width: double.infinity,
          child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              leading: Icon(Icons.build),
              title: Text('\nRegulagens do pneu\n'),
              subtitle: Text('Esta opção irá te mostrar como ajustar o pneu do seu carro/moto para o melhor desempenho e performance.'),
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
                  child: const Text('QUERO CONFERIR'),
                  onPressed: () {Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => TireAdjustment()
                      ),
                    );
                    },
                ),
                // FlatButton(
                //   child: const Text('LISTEN'),
                //   onPressed: () {/* ... */},
                // ),
              ],
            ),
          ],
        ),
      ),
        ),
        
      ],
    );
  }

  Widget _alertManual(BuildContext context) {

   Widget cancelaButton = FlatButton(
    child: Text("Entendi"),
    onPressed:  () { Navigator.of(context).pop();},
  );
  // Widget continuaButton = FlatButton(
  //   child: Text("Continar"),
  //   onPressed:  () {},
  // );

  //configura o AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Manual do usuário"),
    content: Text("O aplicativo tem como objetivo ajudar a regular o seu carro como desejar, seja como de fabrica para uma melhor durabilidade ou para performance do seu carro.\n NÃO NOS RESPONSABILIZAMOS POR QUALQUER DANO AO SEU VEÍCULO."),
    actions: [
      cancelaButton,
      // continuaButton,
    ],
  );

  //exibe o diálogo
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
}